import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NewComponent } from "./new.component";
import { SharedModule } from "app/shared/shared.module";
import { ClarityModule, ClrIconModule } from "@clr/angular";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    SharedModule,
    BrowserAnimationsModule,
    ClarityModule,
    ClrIconModule,
  ],
})
export class NewModule {}
