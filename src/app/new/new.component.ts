import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { ElectronService } from "../core/services/electron/electron.service";
import { SimpleGit } from "simple-git";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-new",
  templateUrl: "./new.component.html",
  styleUrls: ["./new.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewComponent implements OnInit {
  @ViewChild("editor") el: ElementRef;
  @ViewChild("notaPreferences") wNewNoteWiz: ElementRef;

  /**
   * Git component
   *
   * @type {SimpleGit}
   * @memberof HomeComponent
   */
  git: SimpleGit;
  /**
   * Loaded notes
   *
   * @type {Array<NoteDetail>}
   * @memberof HomeComponent
   */
  notes: Array<Note>;

  /**
   * Note creation window visibility
   * visible when set to `true`, and hidden otherwise
   *
   * @type {boolean}
   * @memberof HomeComponent
   */
  wBusy: boolean;

  /**
   * Installed user extension repositories
   *
   * @type {Array<Object>}
   * @memberof HomeComponent
   */
  wNewNoteWizUserRepos: Array<UserRepo>;

  /**
   * Private extension repositories editor visibility,
   * visible when set to `true`
   *
   * @type {boolean}
   * @memberof HomeComponent
   */
  wEditPrivateReposVisible: boolean;

  /**
   * Private extension repositories
   *
   * @type {Array<Object>}
   * @memberof HomeComponent
   */
  wPrivateRepos: Array<UserRepo>;

  /**
   * Currently selected note
   */
  cur: Note;

  /**
   * Notes repository name
   *
   * @type {string}
   * @memberof HomeComponent
   */
  rootDirName: string;

  /**
   * Storage directory
   *
   * @type {string}
   * @memberof HomeComponent
   */
  rootDirPath: string;

  /**
   * Input: title of a new note
   *
   * @type {string}
   * @memberof NewComponent
   */
  iNewNoteTitle: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public electronService: ElectronService
  ) {
    const windowTitle = electronService.remote.getCurrentWindow().title;
    console.warn(windowTitle);
    if (windowTitle === "new notanote") {
      this.router.navigate(["/new"]);
    }

    this.rootDirName = "notanotes";

    this.cur = {
      title: null,
      content: null,
      id: null,
    };
    this.notes = [];

    const app = electronService.remote.app;

    this.rootDirPath = app.getPath("userData");
    console.dir(this.rootDirPath);
    this.rootDirName = this.rootDirPath + "/" + this.rootDirName;

    this.wBusy = true;
    this.wNewNoteWizUserRepos = [];

    this.wEditPrivateReposVisible = false;
    this.wPrivateRepos = [
      {
        title: "Test",
        content: "Hello, world!",
        extensions: [
          {
            title: "Private extension",
            description: "...",
          },
        ],
      },
    ];
  }

  ngOnInit(): void {
    this.electronService.ipcRenderer.on("new-note-data", (event, arg) => {
      console.warn(arg); // prints "pong"
    });
  }

  dataToHome(): void {
    this.electronService.ipcRenderer.send("new-note-data", this.iNewNoteTitle);

    this.wClose();
  }

  wClose(): void {
    this.electronService.remote.getCurrentWindow().close();
  }

  fetchUserRepo = (): Promise<void> =>
    new Promise(() => {
      setTimeout(() => {
        this.wNewNoteWizUserRepos = [];
        this.wNewNoteWizUserRepos.push(
          {
            title: "Example dashboard template",
            content: "@username/dashboard",
          },
          {
            title: "Task template",
            content: "@username/dashboard",
          }
        );
      }, 1500);
    });
}
