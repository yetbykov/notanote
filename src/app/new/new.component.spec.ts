import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { ElectronService } from "../core/services/electron/electron.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { NewComponent } from "./new.component";
describe("NewComponent", () => {
  let component: NewComponent;
  let fixture: ComponentFixture<NewComponent>;
  beforeEach(() => {
    const electronServiceStub = () => ({
      remote: {
        getCurrentWindow: () => ({ title: {} }),
        app: { getPath: () => ({}) },
      },
    });
    const routerStub = () => ({ navigate: (array) => ({}) });
    const activatedRouteStub = () => ({});
    TestBed.configureTestingModule({
      imports: [FormsModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [NewComponent],
      providers: [
        { provide: ElectronService, useFactory: electronServiceStub },
        { provide: Router, useFactory: routerStub },
        { provide: ActivatedRoute, useFactory: activatedRouteStub },
      ],
    });
    fixture = TestBed.createComponent(NewComponent);
    component = fixture.componentInstance;
  });
  it("can load instance", () => {
    expect(component).toBeTruthy();
  });
});
