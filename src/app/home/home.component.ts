import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  MonacoEditorComponent,
  MonacoEditorConstructionOptions,
  MonacoEditorLoaderService,
  MonacoStandaloneCodeEditor,
} from "@materia-ui/ngx-monaco-editor";
import * as fs from "fs";
import * as path from "path";
import { filter, take } from "rxjs/operators";
import simpleGit, { SimpleGit } from "simple-git";
import * as url from "url";

import { ElectronService } from "../core/services";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  @ViewChild(MonacoEditorComponent, { static: false })
  monacoComponent: MonacoEditorComponent;
  editorOptions: MonacoEditorConstructionOptions = {
    theme: "myCustomTheme",
    language: "html",
    roundedSelection: true,
    autoIndent: true,
  };
  code: string;

  @ViewChild("notaPreferences") wNewNoteWiz: ElementRef;

  /**
   * Git
   *
   * @type {SimpleGit}
   * @memberof HomeComponent
   */
  git: SimpleGit;

  /**
   * Notes
   *
   * @type {Array<NoteDetail>}
   * @memberof HomeComponent
   */
  notes: Array<Note>;

  /**
   * Is "New note" window visible
   *
   * @type {boolean}
   * @memberof HomeComponent
   */
  wBusy: boolean;

  /**
   * Installed user extension repositories
   *
   * @type {Array<Object>}
   * @memberof HomeComponent
   */
  wNewNoteWizUserRepos: Array<UserRepo>;

  /**
   * Is "Connected Repositories" window visible
   *
   * @type {boolean}
   * @memberof HomeComponent
   */
  wEditPrivateReposVisible: boolean;

  /**
   * Private extension repositories
   *
   * @type {Array<Object>}
   * @memberof HomeComponent
   */
  wPrivateRepos: Array<UserRepo>;

  /**
   * Current note
   */
  cur: Note;

  /**
   * Notes repository name
   *
   * @type {string}
   * @memberof HomeComponent
   */
  rootDirName: string;

  /**
   * Application data directory
   *
   * @type {string}
   * @memberof HomeComponent
   */
  rootDirPath: string;

  /**
   * Editor
   *
   * @type {EditorJS}
   * @memberof HomeComponent
   */
  new: string;

  menuTree: any[];

  /**
   * Creates an instance of HomeComponent.
   * @param {Router} router Router
   * @param {ActivatedRoute} route Route
   * @param {ElectronService} electronService Electron service
   * @memberof HomeComponent
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private electronService: ElectronService,
    private monacoLoaderService: MonacoEditorLoaderService
  ) {
    const windowTitle = electronService.remote.getCurrentWindow().title;
    console.warn(windowTitle);
    if (windowTitle === "New") {
      this.router.navigate(["/new"]);
    }

    this.rootDirName = "notanotes";

    this.cur = {
      title: null,
      content: null,
      id: null,
    };

    this.resetState();

    const app = electronService.remote.app;
    this.monacoLoaderService.isMonacoLoaded$
      .pipe(
        filter((isLoaded) => isLoaded),
        take(1)
      )
      .subscribe(() => {
        monaco.editor.defineTheme("myCustomTheme", {
          base: "vs-dark", // can also be vs or hc-black
          inherit: true, // can also be false to completely replace the builtin rules
          rules: [
            {
              token: "comment",
              foreground: "000000",
              fontStyle: "italic underline",
            },
            { token: "comment.js", foreground: "008800", fontStyle: "bold" },
            { token: "comment.css", foreground: "0000ff" }, // will inherit fontStyle from `comment` above
          ],
          colors: {},
        });
      });

    this.rootDirPath = app.getPath("userData");
    console.dir(this.rootDirPath);
    this.rootDirName = this.rootDirPath + "/" + this.rootDirName;

    this.wBusy = false;
    this.wNewNoteWizUserRepos = [];

    this.wEditPrivateReposVisible = false;
    this.wPrivateRepos = [
      {
        title: "Test",
        content: "Hello, world!",
        extensions: [
          {
            title: "Private extension",
            description: "...",
          },
        ],
      },
    ];
  }

  ngOnInit(): void {
    try {
      this.repoSync();
    } catch (e) {
      throw new Error("Error syncing repository: " + (e as Error).message);
    }
  }

  ngAfterViewInit(): void {
    //
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  editorInit(editor: MonacoStandaloneCodeEditor) {
    // monaco.editor.setTheme('vs');
    editor.setSelection({
      startLineNumber: 1,
      startColumn: 1,
      endColumn: 50,
      endLineNumber: 3,
    });
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  getCode() {
    return (
      // tslint:disable-next-line: max-line-length
      '<html><!-- // !!! Tokens can be inspected using F1 > Developer: Inspect Tokens !!! -->\n<head>\n	<!-- HTML comment -->\n	<style type="text/css">\n		/* CSS comment */\n	</style>\n	<script type="javascript">\n		// JavaScript comment\n	</' +
      "script>\n</head>\n<body></body>\n</html>"
    );
  }

  /**
   * Synchronizes repo or creates new, if none are found
   *
   * @memberof HomeComponent
   */
  repoSync(): void {
    this.resetState();

    // New installation
    if (!fs.existsSync(this.rootDirName)) {
      console.warn(":: Repository not found in " + this.rootDirName + ".");

      // Create a root directory
      fs.mkdirSync(this.rootDirName);

      this.git = simpleGit(this.rootDirName);

      // Init new repo inside
      this.git
        .clone("https://gitlab.com/yetbykov/notes", this.rootDirName)
        .then((result) => {
          console.info("::  " + this.rootDirName, result);
          this.repoRead();
        });
    } else {
      this.git = simpleGit(this.rootDirName);

      console.info(":: Synchronizing...");

      this.git
        .pull()
        .push()
        .then(() => {
          this.repoRead();
        });
    }
  }

  private repoPull(): void {
    console.info(":: Pulling repository updates");

    this.git.pull().then(() => {
      console.log(":: Pull finished");
      this.resetState();
      this.repoRead();
    });
  }

  private repoPush(): void {
    console.info(":: Pushing any existing changes to remote");

    this.git
      .add(".")
      .commit("Update note(s)")
      .push()
      .then(() => {
        console.log(":: Push finished");
      });
  }

  private resetState(): void {
    console.info(":: Reset current state");
    this.code = this.getCode();

    this.notes = [];
    this.menuTree = [
      // #0
      {
        name: "Search",
        icon: "search",
      },

      // #1
      {
        name: "Notifications",
        icon: "bell",
        expanded: false,
        entries: [
          {
            icon: "file",
            name: "Example note with reminder",
            active: false,
          },
        ],
      },

      // #2
      {
        name: "Saved search",
        icon: "fish",
        expanded: false,
        entries: [],
      },

      // #3
      {
        name: "Pinned",
        icon: "pin",
        expanded: false,
        entries: [],
      },

      // #4
      {
        name: "All notes",
        icon: "file-group",
        expanded: false,
        entries: [],
      },
    ];
  }

  private repoRead(): void {
    console.debug(":: Reading notes from repository");
    const ls = fs.readdirSync(this.rootDirName);
    console.debug(ls);

    for (const filename of ls) {
      if (filename !== ".git") {
        try {
          const a = JSON.parse(
            fs.readFileSync(this.rootDirName + "/" + filename, "utf8")
          ) as Note;
          this.notes.push(a);
          this.menuTree[4].entries.push({
            name: a.title,
            id: a.id,
            icon: null,
            expanded: false,
            entries: [],
          });
        } catch (e) {
          console.warn(`:: Ignoring ${filename}: ${e.message as string}`);
        }
      }
    }
  }

  /**
   * Saves open note to a file
   *
   * @memberof HomeComponent
   */
  noteStore(): void {
    console.info(":: Writing note to file");

    this.cur.content = this.code;

    fs.writeFileSync(
      this.rootDirName + "/" + this.cur.id,
      JSON.stringify(this.cur)
    );
  }

  /**
   * Get note by it's ID
   *
   * @param {string} id Note's ID
   * @memberof HomeComponent
   */
  noteLoad(id: string): void {
    try {
      this.cur = JSON.parse(
        fs.readFileSync(this.rootDirName + "/" + id, "utf8")
      );
      this.code = JSON.stringify(this.cur.content);
    } catch (e) {
      throw Error("Error loading note: " + (e as Error).message);
    }
  }

  /**
   *  New note button handler
   *
   * @returns {void}
   * @memberof HomeComponent
   */
  onNewBtnPressed(): void {
    if (this.wBusy) {
      throw new Error("Busy");
    }

    this.wBusy = true;

    const _proc = this.electronService.remote.process;
    const _args = _proc.argv.slice(1),
      serve = _args.some((val) => val === "--serve");

    let win = new this.electronService.remote.BrowserWindow({
      show: false,
      darkTheme: true,
      resizable: false,
      transparent: true,
      frame: true,
      backgroundColor: "#00000000",
      titleBarStyle: "hiddenInset",
      height: 450,
      width: 864,
      webPreferences: {
        nodeIntegration: true,
        allowRunningInsecureContent: true,
        enableRemoteModule: true,
      },
      title: "New",
      modal: true,
      parent: this.electronService.remote.getCurrentWindow(),
    })
      .on("closed", () => {
        win = null;
        this.wBusy = false;
      })
      .once("ready-to-show", () => {
        win.show();
      });

    if (serve) {
      win.webContents.openDevTools();

      win.loadURL("http://localhost:4200");
    } else {
      win.loadURL(
        url.format({
          pathname: path.join(__dirname, "index.html"),
          protocol: "file:",
          slashes: true,
        })
      );
    }

    this.electronService.ipcRenderer.once("new-note-data", (event, arg) =>
      this.handleNewNoteData(event, arg)
    );
  }

  onPullBtnPressed(): void {
    this.repoPull();
  }

  onPushBtnPressed(): void {
    this.noteStore();
    this.repoPush();
  }

  /**
   * Handle new note data
   *
   * @returns {Note[]} all notes object after creating new note
   * @memberof HomeComponent
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  private handleNewNoteData(
    event: Electron.IpcRendererEvent,
    arg: any
  ): Note[] {
    if (typeof arg !== typeof "") {
      throw new Error("Wrong title type");
    }

    console.info("Create new note = {", arg, "}");

    const el: Note = {
      id: this.getUniqueID(),
      title: arg,
      content: null,
    };

    this.cur = el;

    this.code = this.getCode();

    return this.notes;
  }

  /**
   * Generates and returns the ID
   *
   * @returns {string} Unique ID
   * @memberof HomeComponent
   */
  getUniqueID(): string {
    return Math.random().toString(36).substr(2, 9);
  }

  fetchUserRepo = (): Promise<void> =>
    new Promise(() => {
      setTimeout(() => {
        this.wNewNoteWizUserRepos = [];
        this.wNewNoteWizUserRepos.push(
          {
            title: "Example dashboard template",
            content: "@username/dashboard",
          },
          {
            title: "Task template",
            content: "@username/dashboard",
          }
        );
      }, 1500);
    });

  /**
   *  Home
   *
   * @param {string} directoryName
   * @param {string} fileName
   * @memberof HomeComponent
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  openFile(directoryName: string, fileName: string): void {
    console.info(
      ` :: [HomeComponent]: requested file ${fileName} from directory ${directoryName}`
    );
  }
}
