import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule } from "./home-routing.module";

import { HomeComponent } from "./home.component";
import { SharedModule } from "../shared/shared.module";
import { ClarityModule, ClrIconModule } from "@clr/angular";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MonacoEditorModule, MONACO_PATH } from "@materia-ui/ngx-monaco-editor";
import { MarkdownModule } from "ngx-markdown";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    BrowserAnimationsModule,
    ClarityModule,
    ClrIconModule,
    MonacoEditorModule,
    MarkdownModule.forChild(),
  ],
  providers: [
    {
      provide: MONACO_PATH,
      useValue: "https://unpkg.com/monaco-editor@0.19.3/min/vs",
    },
  ],
  declarations: [HomeComponent],
  bootstrap: [HomeComponent],
})
export class HomeModule {}
