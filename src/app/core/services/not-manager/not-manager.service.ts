import { Injectable } from "@angular/core";
import cloneDeep from "lodash.clonedeep";

/**
 * NotManagerService configuration interface
 *
 * @interface INotConfig
 */
interface INotConfig {
  remoteAddress: string;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const DefaultNotConfig: INotConfig = {
  remoteAddress: null,
};

@Injectable({
  providedIn: "root",
})
export class NotManagerService {
  /**
   * Service configuration
   *
   * @private
   * @type {INotConfig}
   * @memberof NotManagerService
   */
  private config: INotConfig;

  /**
   * Creates an instance of NotManagerService
   * @memberof NotManagerService
   */
  constructor() {
    this.configReset();
  }

  /**
   * Load default configuration
   *
   * @returns {INotConfig} current configuration
   * @memberof NotManagerService
   */
  configReset(): INotConfig {
    console.debug("NotManagerService -> Loading default configuration");
    this.config = cloneDeep(DefaultNotConfig);
    return this.config;
  }

  /**
   * Load user configuration
   *
   * @returns {INotConfig} current configuration
   * @memberof NotManagerService
   */
  configLoadUser(): INotConfig {
    console.debug("NotManagerService -> Loading user configuration");
    this.config = {
      remoteAddress: "123",
    };
    return this.config;
  }
}
