import { TestBed } from '@angular/core/testing';
import { NotManagerService } from './not-manager.service';

describe('NotManagerService', () => {
  let service: NotManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [NotManagerService] });
    service = TestBed.get(NotManagerService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});
