import { Injectable } from "@angular/core";

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote } from "electron";
import * as childProcess from "child_process";
import * as fs from "fs";

/**
 * Electron service
 *
 * @export
 * @class ElectronService
 */
@Injectable({
  providedIn: "root",
})
export class ElectronService {
  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;

  get isElectron(): boolean {
    return !!(window && window.process && window.process.type);
  }

  constructor() {
    this.importElectronRequirements();
  }

  /**
   * Conditional imports for Electron
   * @param {boolean} [isElectron=this.isElectron]
   * @param {any} [win=window]
   * @returns {boolean}
   * @memberof ElectronService
   */
  importElectronRequirements(
    isElectron: boolean = this.isElectron,
    win: any = window
  ): boolean {
    if (isElectron) {
      this.ipcRenderer = win.require("electron").ipcRenderer;
      this.webFrame = win.require("electron").webFrame;
      this.remote = win.require("electron").remote;

      this.childProcess = win.require("child_process");
      this.fs = win.require("fs");

      return true;
    }
  }
}
