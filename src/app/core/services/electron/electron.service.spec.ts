import { TestBed } from "@angular/core/testing";

import { ElectronService } from "./electron.service";

describe("ElectronService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: ElectronService = TestBed.inject(ElectronService);
    expect(service).toBeTruthy();
  });

  it("should define isElectron", () => {
    const service: ElectronService = TestBed.inject(ElectronService);
    expect(service.isElectron).toBeDefined();
  });

  it("should conditionally import electron requirements", () => {
    const service: ElectronService = TestBed.inject(ElectronService);

    const windowMock = {
      require() {
        return {
          ipcRenderer: {},
          webFrame: {},
          remote: {},
        };
      },
    };

    expect(service.importElectronRequirements(true, windowMock)).toBeTrue();
  });
});
