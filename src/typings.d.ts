/* SystemJS module definition */
declare var nodeModule: NodeModule;

interface NodeModule {
  id: string;
}

interface Window {
  process: any;
  require: any;
}

/**
 * Note base interface
 *
 * @interface Note
 */
interface Note {
  /**
   * Unique ID
   *
   * @type {string}
   * @memberof Note
   */
  id: string;

  /**
   * Note title, also may be used as filename
   *
   * @type {string}
   * @memberof Note
   */
  title: string;

  /**
   * Note content
   *
   * @type {*}
   * @memberof Note
   */
  content: any;
}

/**
 * User repositories contain various custom
 * extensions (ex.: template) for the application
 *
 * @interface UserRepo
 */
interface UserRepo {
  /**
   * URI of the repo
   *
   * @type {string}
   * @memberof UserRepo
   */
  uri?: string;
  /**
   * Repository title
   *
   * @type {string}
   * @memberof UserRepo
   */
  title: string;
  /**
   * TODO: remove
   *
   * @type {string}
   * @memberof UserRepo
   */
  content: string;
  extensions?: Array<UserRepoExtension>;
}

/**
 * Extension
 *
 * @interface UserRepoExtension
 */
interface UserRepoExtension {
  /**
   * Extension name
   *
   * @type {string}
   * @memberof UserRepoExtension
   */
  title: string;
  /**
   * Extension description
   *
   * @type {string}
   * @memberof UserRepoExtension
   */
  description: string;
}
